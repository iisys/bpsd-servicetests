package al.elass.dinnersubscription;

import al.elass.dinnersubscription.DinnerSubscriptionServiceStub.Attendee;
import al.elass.dinnersubscription.DinnerSubscriptionServiceStub.Attendees_type0;
import al.elass.dinnersubscription.DinnerSubscriptionServiceStub.DinnerStatistics;
import al.elass.dinnersubscription.DinnerSubscriptionServiceStub.GetStatistics;
import al.elass.dinnersubscription.DinnerSubscriptionServiceStub.GetStatisticsResponse;
import al.elass.dinnersubscription.DinnerSubscriptionServiceStub.GetSubscription;
import al.elass.dinnersubscription.DinnerSubscriptionServiceStub.GetSubscriptionResponse;
import al.elass.dinnersubscription.DinnerSubscriptionServiceStub.People;
import al.elass.dinnersubscription.DinnerSubscriptionServiceStub.Person;
import al.elass.dinnersubscription.DinnerSubscriptionServiceStub.Subscription;
import al.elass.dinnersubscription.DinnerSubscriptionServiceStub.UpdateSubscription;
import al.elass.dinnersubscription.DinnerSubscriptionServiceStub.UpdateSubscriptionResponse;

import java.text.SimpleDateFormat;

/**
 * The test case for the DinnerSubscription service.
 * 
 * @author Karim El Assal
 * @version 1.0
 */
public class DinnerSubscriptionServiceTest extends junit.framework.TestCase {
    
    /**
     * Name of a random, non-existing person.
     */
    private static final String PERSON = "Resident #" + randomString(10);
    
    /**
     * Date string of an unused date, which we use to test subscribing.
     */
    private static final String DATESTR = "1992-01-16";
    
    /**
     * The DinnerSubscription service stub.
     */
    private DinnerSubscriptionServiceStub stub;
    
    /**
     * Sets up the test environment.
     */
    @Override
    protected void setUp() throws Exception {
        stub = new DinnerSubscriptionServiceStub();
        reset();
    }
    
    /**
     * Tears down the test environment.
     */
    @Override
    protected void tearDown() throws Exception {
        reset();
    }

    /**
     * Test fetching statistics.
     */
    public void testgetStatistics() throws Exception {
        GetStatistics getStatisticsRequest = new GetStatistics();
        GetStatisticsResponse response = stub.getStatistics(getStatisticsRequest);
        
        // Check result.
        assertNotNull(response);
        DinnerStatistics stats = response.getDinnerStatistics();
        assertNotNull(stats);
        assertNotNull(stats.getFrom());
        assertNotNull(stats.getTo());
        People people = stats.getPeople();
        assertNotNull(people);
        
        if (people.getPerson() != null) {
            for (Person person : people.getPerson()) {
                assertNotNull(person.getName());
                assertEquals(person.getRatio(), 
                    ((float) person.getDinnersCooked() / person.getDinnersAttended()));
            }
        }
    }

    /**
     * Test: update subscription, check response. Fetch subscription, check response.
     * 
     * <p>Test   1: persons=0, willCook=false (set baseline)
     *    Result 1: persons=0, willCook=false
     * 
     * <p>Test   2: persons=0, willCook=true (impossible scenario)
     *    Result 2: persons=0, willCook=false
     * 
     * <p>Test   3: persons=-1, willCook=true (impossible scenario)
     *    Result 3: persons=0, willCook=false
     * 
     * <p>Test   4: persons=3, willCook=true (correct values)
     *    Result 4: persons=3, willCook=true
     */
    public void testupdateSubscription() throws Exception {

        GetSubscription getRequest = new GetSubscription();
        getRequest.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(DATESTR));
        UpdateSubscriptionResponse updResponse;
        
        reset();
        
        // Test 1.
        updResponse = tryUpdatingDefaultPerson(                0, false);
        assertNotNull(updResponse);
        evaluateSubscription(updResponse.getSubscription(), 0, false);
        GetSubscriptionResponse getResponse = stub.getSubscription(getRequest);
        assertNotNull(getResponse);
        evaluateSubscription(getResponse.getSubscription(), 0, false);
        
        // Test 2.
        updResponse = tryUpdatingDefaultPerson(                0, true);
        assertNotNull(updResponse);
        evaluateSubscription(updResponse.getSubscription(), 0, false);
        getResponse = stub.getSubscription(getRequest);
        assertNotNull(getResponse);
        evaluateSubscription(getResponse.getSubscription(), 0, false);
        
        // Test 3.
        updResponse = tryUpdatingDefaultPerson(              -1, true);
        assertNotNull(updResponse);
        evaluateSubscription(updResponse.getSubscription(), 0, false);
        getResponse = stub.getSubscription(getRequest);
        assertNotNull(getResponse);
        evaluateSubscription(getResponse.getSubscription(), 0, false);
        
        // Test 4.
        updResponse = tryUpdatingDefaultPerson(                3, true);
        assertNotNull(updResponse);
        evaluateSubscription(updResponse.getSubscription(), 3, true);
        getResponse = stub.getSubscription(getRequest);
        assertNotNull(getResponse);
        evaluateSubscription(getResponse.getSubscription(), 3, true);
                
    }
    
    /**
     * Resets all subscriptions on the test date.
     */
    private void reset() throws Exception {
        GetSubscription getSubscriptionRequest = new GetSubscription();
        getSubscriptionRequest.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(DATESTR));
        GetSubscriptionResponse response = stub.getSubscription(getSubscriptionRequest);
        Subscription subscription = response.getSubscription();
        if (subscription.getAttendees().getAttendee() != null) {
            for (Attendee a : subscription.getAttendees().getAttendee()) {
                a.setPersons(0);
            }
        }
        UpdateSubscription updateRequest = new UpdateSubscription();
        updateRequest.setSubscription(subscription);
        stub.updateSubscription(updateRequest);
    }
    
    /**
     * Try to update a subscription, for the person defined in <code>PERSON</code>.
     * 
     * @param persons   the amount of persons that PERSON will eat with (its amount of attendees)
     * @param willCook  whether PERSON will cook or not
     * @return          the service response
     */
    private UpdateSubscriptionResponse tryUpdatingDefaultPerson(int persons, boolean willCook) 
            throws Exception {
        
        // Build attendee list (consisting of one person).
        Attendee[] attendee = new Attendee[1];
        attendee[0] = new Attendee();
        attendee[0].setName(PERSON);
        attendee[0].setPersons(persons);
        attendee[0].setWillCook(willCook);
        Attendees_type0 attendeeList = new Attendees_type0();
        attendeeList.setAttendee(attendee);
        // Fill request object.
        Subscription subscription = new Subscription();
        subscription.setAttendees(attendeeList);
        subscription.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(DATESTR));
        UpdateSubscription updateRequest = new UpdateSubscription();
        updateRequest.setSubscription(subscription);
        
        // Get response.
        return stub.updateSubscription(updateRequest);
    }
    
    /**
     * Evaluate a subscription according to passed values using asserts.
     * 
     * @param subscr    the Subscription instance to evaluate
     * @param persons   the amount of persons that PERSON will eat with (its amount of attendees)
     * @param willCook  whether PERSON will cook or not
     */
    private void evaluateSubscription(Subscription subscr, int persons, boolean willCook) 
            throws Exception {
        assertNotNull(subscr);
        assertNotNull(subscr.getDate());
        assertNotNull(subscr.getAttendees());
        assertTrue(new SimpleDateFormat("yyyy-MM-dd").parse(DATESTR).equals(subscr.getDate()));

        Attendee[] attendee = subscr.getAttendees().getAttendee();
        if (persons > 0) {
            assertNotNull(attendee);
            assertEquals(1, attendee.length);
            assertEquals(PERSON, attendee[0].getName());
            assertEquals(persons, attendee[0].getPersons());
            assertEquals(willCook, attendee[0].getWillCook());
        }
    }
    
    /**
     * Generates a random string of integers of a specific length.
     * 
     * @param length    the length of the generated string
     * @return          the generated random string of integers
     */
    private static String randomString(int length) {
        String str = "";
        for (int i = 0; i < length; i++) {
            str += randomInt(0, 9);
        }
        return str;
    }

    /**
     * Generates a random integer.
     * 
     * @param min   the lower bound
     * @param max   the upper bound
     * @return      a random integer
     */
    private static int randomInt(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));
    }

}
