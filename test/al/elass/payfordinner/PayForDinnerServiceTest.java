package al.elass.payfordinner;

import al.elass.accountbalance.AccountBalanceServiceStub;
import al.elass.accountbalance.AccountBalanceServiceStub.AddEntity;
import al.elass.accountbalance.AccountBalanceServiceStub.AddMutation;
import al.elass.accountbalance.AccountBalanceServiceStub.Entity;
import al.elass.accountbalance.AccountBalanceServiceStub.GetBalance;
import al.elass.accountbalance.AccountBalanceServiceStub.GetBalanceResponse;
import al.elass.accountbalance.AccountBalanceServiceStub.MutationEntry;
import al.elass.accountbalance.AccountBalanceServiceStub.Share_type0;
import al.elass.accountbalance.AccountBalanceServiceStub.Shares_type0;
import al.elass.dinnersubscription.DinnerSubscriptionServiceStub;
import al.elass.dinnersubscription.DinnerSubscriptionServiceStub.Attendee;
import al.elass.dinnersubscription.DinnerSubscriptionServiceStub.Attendees_type0;
import al.elass.dinnersubscription.DinnerSubscriptionServiceStub.GetSubscription;
import al.elass.dinnersubscription.DinnerSubscriptionServiceStub.GetSubscriptionResponse;
import al.elass.dinnersubscription.DinnerSubscriptionServiceStub.Subscription;
import al.elass.dinnersubscription.DinnerSubscriptionServiceStub.UpdateSubscription;
import al.elass.payfordinner.PayForDinnerServiceStub.ProcessPayment;
import al.elass.payfordinner.PayForDinnerServiceStub.ProcessPaymentResponse;

import java.text.SimpleDateFormat;

/**
 * The test case for the PayForDinner service.
 * 
 * @author Karim El Assal
 * @version 1.0
 */
public class PayForDinnerServiceTest extends junit.framework.TestCase {
    
    /**
     * The dinner price used in the tests.
     */
    private static final float DINNERPRICE = 6.60f;

    /**
     * Name of an existing resident.
     */
    private static final String RESIDENT1 = "Resident #1";
    
    /**
     * Name of an existing resident.
     */
    private static final String RESIDENT2 = "Resident #2";
    
    /**
     * Name of a non-existing resident.
     */
    private static final String RESIDENTU = "Resident #" + randomString(4);

    /**
     * Subscription date only used for testing.
     */
    private static final String DATE1 = "1992-01-16";

    /**
     * Subscription date only used for testing.
     */
    private static final String DATE2 = "1992-01-17";

    /**
     * Subscription date only used for testing.
     */
    private static final String DATE3 = "1992-01-18";

    /**
     * The amount of attendees for date 1 and resident 1.
     */
    private static final int D1_R1_P = 2;

    /**
     * The amount of attendees for date 1 and resident 2.
     */
    private static final int D1_R2_P = 1;

    /**
     * The amount of attendees for date 2 and resident 1.
     */
    private static final int D2_R1_P = 1;

    /**
     * The amount of attendees for date 2 and the unknown resident.
     */
    private static final int D2_RU_P = 1;
    
    /**
     * The PayForDinner service stub.
     */
    private PayForDinnerServiceStub stub; 
    
    /**
     * Sets up the test environment: make sure the residents exist, reset the subscriptions 
     * on the testing dates and set subscriptions on those dates to specific values.
     */
    @Override
    protected void setUp() throws Exception {
        stub = new PayForDinnerServiceStub();
        
        // Make sure they exist.
        ensureEntity(RESIDENT1);
        ensureEntity(RESIDENT2);

        // Reset.
        reset(DATE1);
        reset(DATE2);
        reset(DATE3);
        
        // Set baseline
        setSubscription(DATE1, RESIDENT1, D1_R1_P, false);
        setSubscription(DATE1, RESIDENT2, D1_R2_P, false);
        setSubscription(DATE2, RESIDENT1, D2_R1_P, false);
        setSubscription(DATE2, RESIDENTU, D2_RU_P, false);
    }
    
    /**
     * Tears down the test environment by resetting all subscriptions on the testing dates.
     */
    @Override
    protected void tearDown() throws Exception {
        reset(DATE1);
        reset(DATE2);
        reset(DATE3);
    }

    /**
     * Test the ProcessPayment operation of the PayForDinner service.
     */
    public void testprocessPayment() throws Exception {

        float balance1 = getBalance(RESIDENT1);
        float balance2 = getBalance(RESIDENT2);
        
        String result;
        
        // Test 1: (fail) unknown payer.
        result = processPayment(DATE1, RESIDENTU, DINNERPRICE);
        assertTrue(notNullAndContains("Error: payer is not", result));
        
        // Test 2: (fail) unknown payee.
        result = processPayment(DATE2, RESIDENT1, DINNERPRICE);
        assertTrue(notNullAndContains("Error: dinner attendee", result));
        
        // Test 3: (fail) no dinner attendees on that date.
        result = processPayment(DATE3, RESIDENT1, DINNERPRICE);
        System.out.println(result);
        assertTrue(notNullAndContains("Error: There are no dinner attendees", result));
        
        // Intermediary test: confirm that nothing has changed.
        assertEquals(balance1, getBalance(RESIDENT1));
        assertEquals(balance2, getBalance(RESIDENT2));
        
        // Test 4: (success) correct values.
        result = processPayment(DATE1, RESIDENT1, DINNERPRICE);
        assertTrue(notNullAndContains("Success!", result));
        assertEquals(balance1 + DINNERPRICE 
                - DINNERPRICE * D1_R1_P / (D1_R1_P + D1_R2_P), getBalance(RESIDENT1));
        assertEquals(balance2                 
                - DINNERPRICE * D1_R2_P / (D1_R1_P + D1_R2_P), getBalance(RESIDENT2));
        
        // Wrap up: add balance to cancel out the altered balance.
        alterBalance(RESIDENT1, -1 * (DINNERPRICE - DINNERPRICE * D1_R1_P / (D1_R1_P + D1_R2_P)));
        alterBalance(RESIDENT2, -1 * (          0 - DINNERPRICE * D1_R2_P / (D1_R1_P + D1_R2_P)));
        
    }
    
    /**
     * Check if a variable is not null and contains a specific value.
     * @param needle    value to search for
     * @param haystack  variable to check
     * @return  true if <code>haystack</code> is not null and contains <code>needle</code>
     */
    private boolean notNullAndContains(String needle, String haystack) {
        return haystack != null && haystack.contains(needle);
    }
    
    /**
     * Execute a ProcessPayment operation on the PayForDinner service.
     * 
     * @param date  the date string in format yyyy-MM-dd
     * @param payer the person who paid
     * @param price the dinner price
     * @return the result of the call to the service
     */
    private String processPayment(String date, String payer, float price) throws Exception {
        ProcessPayment request = new ProcessPayment();
        request.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(date));
        request.setPayer(payer);
        request.setPrice(price);
        
        ProcessPaymentResponse response = stub.processPayment(request);
        return response.getResult();
    }
    
    /**
     * Retrieve the account balance from the AccountBalance service.
     * 
     * @param entityStr the entity from which to retrieve the belance
     * @return  the balance
     */
    private float getBalance(String entityStr) throws Exception {
        AccountBalanceServiceStub ab = new AccountBalanceServiceStub();

        Entity entity = new Entity();
        entity.setEntity(entityStr);
        
        GetBalance request = new GetBalance();
        request.setEntity(entity);
        
        GetBalanceResponse response = ab.getBalance(request);
        return response.getBalance().getValue();
    }
    
    /**
     * Makes sure <code>entityStr</code> is an existing entity in the AccountBalance
     * service by trying to add it.
     * 
     * @param entityStr the entity
     */
    private void ensureEntity(String entityStr) throws Exception {
        AccountBalanceServiceStub ab = new AccountBalanceServiceStub();

        Entity entity = new Entity();
        entity.setEntity(entityStr);
        
        AddEntity request = new AddEntity();
        request.setEntity(entity);

        ab.addEntity(request);
    }
    
    /**
     * Resets all subscriptions on a specific date.
     * 
     * @param date the date in format yyyy-MM-dd
     */
    private void reset(String date) throws Exception {
        DinnerSubscriptionServiceStub ds = new DinnerSubscriptionServiceStub();
        GetSubscription getSubscriptionRequest = new GetSubscription();
        getSubscriptionRequest.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(date));
        GetSubscriptionResponse response = ds.getSubscription(getSubscriptionRequest);
        Subscription subscription = response.getSubscription();
        if (subscription.getAttendees().getAttendee() != null) {
            for (Attendee a : subscription.getAttendees().getAttendee()) {
                a.setPersons(0);
            }
        }
        UpdateSubscription updateRequest = new UpdateSubscription();
        updateRequest.setSubscription(subscription);
        ds.updateSubscription(updateRequest);
    }
    
    /**
     * Sets a subscription by using the DinnerSubscription service.
     * 
     * @param date      the dinner date
     * @param resident  the resident
     * @param persons   the amount of persons the resident will eat with (amount of attendees)
     * @param willCook  whether the resident will cook or not
     */
    private void setSubscription(String date, String resident, int persons, boolean willCook) 
            throws Exception {
        final DinnerSubscriptionServiceStub ds = new DinnerSubscriptionServiceStub();
        
        Attendee[] attendee = new Attendee[1];
        attendee[0] = new Attendee();
        attendee[0].setName(resident);
        attendee[0].setPersons(persons);
        attendee[0].setWillCook(willCook);
        
        Attendees_type0 attendees = new Attendees_type0();
        attendees.setAttendee(attendee);
        
        Subscription subscription = new Subscription();
        subscription.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(date));
        subscription.setAttendees(attendees);
        
        UpdateSubscription request = new UpdateSubscription();
        request.setSubscription(subscription);
        
        ds.updateSubscription(request);
    }
    
    /**
     * Adds an amount to the balance of an entity by using the AccountBalance service, 
     * with the purpose of rectifying any changes to the balance made by the tests.
     * 
     * @param entityStr the entity
     * @param amount    the amount to add
     */
    private void alterBalance(String entityStr, float amount) throws Exception {
        final AccountBalanceServiceStub ab = new AccountBalanceServiceStub();

        Entity entity = new Entity();
        entity.setEntity(entityStr);
        
        Shares_type0 shares = new Shares_type0();
        shares.setShare(new Share_type0[0]);
        
        MutationEntry entry = new MutationEntry();
        entry.setMutatedBy(entity);
        entry.setValue(amount);
        entry.setDescription("System test side effect rectification");
        entry.setShares(shares);
        
        AddMutation request = new AddMutation();
        request.setEntry(entry);

        ab.addMutation(request);
    }
    
    /**
     * Generates a random string of integers of a specific length.
     * 
     * @param length    the length of the generated string
     * @return          the generated random string of integers
     */
    private static String randomString(int length) {
        String str = "";
        for (int i = 0; i < length; i++) {
            str += randomInt(0, 9);
        }
        return str;
    }

    /**
     * Generates a random integer.
     * 
     * @param min   the lower bound
     * @param max   the upper bound
     * @return      a random integer
     */
    private static int randomInt(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));
    }

}
